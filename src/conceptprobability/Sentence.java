package conceptprobability;

/**
 *
 * @author TOSHIBA PC
 */
public class Sentence
{
    private String text;
    private String[] words;
    public Sentence(String text, String[] words)
    {
        this.text = text;
        this.words = new String[words.length];
        for (int i = 0; i < words.length; i++) {
            this.words[i] = words[i].toLowerCase();
        }
    }
    public String getText()
    {
        return this.text;
    }
    public String[] getWords()
    {
        return this.words;
    }
    public int getLength()
    {
        return this.text.split(" ").length;
    }
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        for (String w : words) {
            sb.append(w).append(" ");
        }
        return sb.toString();
    }
}
