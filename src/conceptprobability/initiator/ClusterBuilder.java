package conceptprobability.initiator;

import IndonesianNLP.IndonesianSentenceDetector;
import IndonesianNLP.IndonesianSentenceFormalization;
import IndonesianNLP.IndonesianSentenceTokenizer;
import IndonesianNLP.IndonesianStemmer;
import conceptprobability.Cluster;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import weka.core.Instances;
import weka.core.tokenizers.NGramTokenizer;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.StringToWordVector;

/**
 *
 * @author TOSHIBA PC
 */
public class ClusterBuilder
{
    public Map<String, List<String>> articles;
    
    /* Constant variable */
    private static final boolean STEMMING = false;
    private static final String CORPUS_FILE_NAME = "resource/conceptprobability/Corpus.arff";
    private static final String CORPUS_PROCESSED_FILE_NAME = "resource/conceptprobability/CorpusProcessed.txt";
    private static final String MI_FILE_NAME = "resource/conceptprobability/MutualInformation.txt";
    private static final String CLUSTER_FILE_NAME = "resource/conceptprobability/Cluster.txt";
    private final int NUM_CLUSTERS = 1200;
    private final int NUM_DOCS = 3000;
    private final int NUM_DOCS_PER_CATEGORY = 300;
    private final String[] CATEGORIES = {
        "Class-Pendidikan",
        "Class-Politik",
        "Class-HukumKriminal",
        "Class-SosialBudaya",
        "Class-Olahraga",
        "Class-TeknologiSains",
        "Class-Hiburan",
        "Class-EkonomiBisnis",
        "Class-Kesehatan",
        "Class-BencanaKecelakaan"
    };
    
    public void getNewsArticles() throws Exception
    {
        System.out.println("Getting news articles...");
        articles = new HashMap<String, List<String>>();
        
        Class.forName("com.mysql.jdbc.Driver");
        Connection connect = DriverManager.getConnection("jdbc:mysql://localhost:3306/newest_news?"
                                            + "user=root&password=dininta");
        Statement statement = connect.createStatement();
        ResultSet resultSet = statement.executeQuery("select * from training");
        
        for (String category : this.CATEGORIES) {
            articles.put(category, new ArrayList<String>());
        }

        while (resultSet.next()) {
            if (resultSet.getInt("Class-Pendidikan") == 1) {
                articles.get("Class-Pendidikan").add(resultSet.getString("FULL_TEXT"));
            } else if (resultSet.getInt("Class-Politik") == 1) {
                articles.get("Class-Politik").add(resultSet.getString("FULL_TEXT"));
            } else if (resultSet.getInt("Class-HukumKriminal") == 1) {
                articles.get("Class-HukumKriminal").add(resultSet.getString("FULL_TEXT"));
            } else if (resultSet.getInt("Class-SosialBudaya") == 1) {
                articles.get("Class-SosialBudaya").add(resultSet.getString("FULL_TEXT"));
            } else if (resultSet.getInt("Class-Olahraga") == 1) {
                articles.get("Class-Olahraga").add(resultSet.getString("FULL_TEXT"));
            } else if (resultSet.getInt("Class-TeknologiSains") == 1) {
                articles.get("Class-TeknologiSains").add(resultSet.getString("FULL_TEXT"));
            } else if (resultSet.getInt("Class-Hiburan") == 1) {
                articles.get("Class-Hiburan").add(resultSet.getString("FULL_TEXT"));
            } else if (resultSet.getInt("Class-EkonomiBisnis") == 1) {
                articles.get("Class-EkonomiBisnis").add(resultSet.getString("FULL_TEXT"));
            } else if (resultSet.getInt("Class-Kesehatan") == 1) {
                articles.get("Class-Kesehatan").add(resultSet.getString("FULL_TEXT"));
            } else if (resultSet.getInt("Class-BencanaKecelakaan") == 1) {
                articles.get("Class-BencanaKecelakaan").add(resultSet.getString("FULL_TEXT"));
            }
        }
    }
    
    public void createCorpusArff() throws IOException, ClassNotFoundException, SQLException
    {
        IndonesianSentenceDetector stDetector = new IndonesianSentenceDetector();
        IndonesianSentenceFormalization formalizer = new IndonesianSentenceFormalization();
        IndonesianSentenceTokenizer tokenizer = new IndonesianSentenceTokenizer();
        IndonesianStemmer stemmer = new IndonesianStemmer();
        formalizer.initStopword();
        
        System.out.println("Creating corpus arff...");
        try (Writer bw = new BufferedWriter(new FileWriter(CORPUS_FILE_NAME))) {
            bw.write("@relation complaint\n\n");
            bw.write("@attribute text string\n");
            bw.write("@attribute __class {Class-Pendidikan,Class-Politik,Class-HukumKriminal,Class-SosialBudaya,Class-Olahraga,Class-TeknologiSains,Class-Hiburan,Class-EkonomiBisnis,Class-Kesehatan,Class-BencanaKecelakaan}\n");
            bw.write("\n@data\n\n");
            
            Iterator it = articles.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry)it.next();
                String key = (String) pair.getKey();
                List<String> val = (List<String>) pair.getValue();
                for (int i = 0; i < this.NUM_DOCS_PER_CATEGORY; i++) {
                    String article = "";
                    List<String> rawSentences = stDetector.splitSentence(val.get(i));
                    for (String sentence : rawSentences) {
                        // Tokenization
                        List<String> tokens = tokenizer.tokenizeSentence(sentence);
                        // Delete stop words
                        String finalize = formalizer.deleteStopword(String.join(" ", tokens));
                        // Retokenization
                        tokens.clear();
                        tokens = tokenizer.tokenizeSentence(finalize);
                        // Delete tokens which are not a word
                        Iterator<String> iter = tokens.iterator();
                        while (iter.hasNext()) {
                            String token = iter.next();
                            if (!token.matches("[a-zA-Z]+")) iter.remove();
                        }
                        // Stemming
                        if (this.STEMMING) {
                            for (int j = 0; j < tokens.size(); j++) {
                                tokens.set(j, stemmer.stem(tokens.get(j)));
                            }
                        }
                        
                        StringBuilder sb = new StringBuilder();
                        for (String token : tokens) {
                            sb.append(token).append(" ");
                        }
                        article = article.concat(sb.toString()).concat(" ");
                    }
                    bw.write("'" + article + "',");
                    bw.write(key + "\n");
                }
                it.remove();
            }
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public void calculateWordFrequency() throws FileNotFoundException, IOException, Exception
    {
        BufferedReader reader = new BufferedReader(new FileReader(CORPUS_FILE_NAME));
        Instances data = new Instances(reader);
        data.setClassIndex(data.numAttributes()-1);
        reader.close();
        System.out.println("Num of instances: " + data.numInstances());
        
        // Set the tokenizer
        NGramTokenizer tokenizer = new NGramTokenizer();
        tokenizer.setNGramMinSize(1);
        tokenizer.setNGramMaxSize(1);
        tokenizer.setDelimiters(" ");
        
        // Set the filter
        StringToWordVector filter = new StringToWordVector();
        filter.setAttributeIndicesArray(new int[]{0});
        filter.setOutputWordCounts(true);
        filter.setTokenizer(tokenizer);
        filter.setInputFormat(data);
        filter.setWordsToKeep(1000000);
        filter.setLowerCaseTokens(true);
        filter.setTFTransform(false);
        filter.setIDFTransform(false);

        // Filter the input instances into the output ones
        System.out.println("Filtering instances...");
        Instances outputInstances = Filter.useFilter(data, filter);
        System.out.println("Num of words: " + outputInstances.numAttributes());
        
        // Write num of docs containing every word
        System.out.println("Writing word frequency to file...");
        try (Writer bw = new BufferedWriter(new FileWriter(CORPUS_PROCESSED_FILE_NAME))) {
            for (int i = 0; i < outputInstances.numAttributes(); i++) {
                int[] numDocs = new int[CATEGORIES.length];
                for (int j = 0; j < numDocs.length; j++) {
                    numDocs[j] = 0;
                }
                for (int j = 0; j < outputInstances.numInstances(); j ++) {
                    if (outputInstances.instance(j).value(i) > 0) {
                        int idx = (int) outputInstances.instance(j).classValue();
                        numDocs[idx]++;
                    }
                }
                bw.write(outputInstances.attribute(i).name());
                for (int j = 0; j < numDocs.length; j++) {
                    bw.write("\t" + numDocs[j]);
                }
                bw.write("\n");
            }
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public void calculateMI() throws Exception
    {
        System.out.println("Calculating MI for all words...");
        Map<String, Double> mi = new LinkedHashMap<String, Double>();
        BufferedReader reader = new BufferedReader(new FileReader(CORPUS_PROCESSED_FILE_NAME));
        String line = null;
        while ((line = reader.readLine()) != null ) {
            String[] data = line.split("\t");
            int[] p = new int[data.length-1];
            for (int i = 0; i < p.length; i++) {
                p[i] = Integer.valueOf(data[i+1]);
            }
            mi.put(line, MICalculator.computeMaxMI(p, NUM_DOCS_PER_CATEGORY , NUM_DOCS));
        }
        reader.close();
        
        // Sorting
        System.out.println("Sorting word by MI...");
        LinkedHashMap<String, Double> sortedMi = mi.entrySet()
            .stream()
            .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
            .collect(Collectors.toMap(
                Map.Entry::getKey,
                Map.Entry::getValue,
                (a, b) -> a,
               LinkedHashMap::new
            ));
        mi.clear();
        
        // Write to file
        System.out.println("Writing MI to text file...");
        try (Writer bw = new BufferedWriter(new FileWriter(MI_FILE_NAME))) {
            Iterator it = sortedMi.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry)it.next();
                String key = (String) pair.getKey();
                double val = (double) pair.getValue();
                bw.write(key + "\t" + val + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public void buildCluster() throws Exception
    {
        System.out.println("Building cluster...");
        List<Cluster> clusters = new ArrayList<Cluster>();
        
        BufferedReader reader = new BufferedReader(new FileReader(MI_FILE_NAME));
        String line = null;int m=0;
        while ((line = reader.readLine()) != null ) { System.out.println(m);m++;
            String[] data = line.split("\t");
            int[] p = new int[data.length-2];
            for (int i = 0; i < p.length; i++) {
                p[i] = Integer.valueOf(data[i+1]);
            }
            
            if (clusters.size() < NUM_CLUSTERS) {
                Cluster cluster = new Cluster(data[0], p);
                clusters.add(cluster);
            } else {
                // Find two clusters with lowest klDivergence
                double minDivergence = Double.MAX_VALUE;
                int firstIdx = 0, secondIdx = 0;
                for (int i = 0; i < clusters.size()-1; i++) {
                    for (int j = i+1; j < clusters.size(); j++) {
                        double divergence = KLDivergence.computeJensenShannonKLD(clusters.get(i), clusters.get(j));
                        if (divergence < minDivergence) {
                            minDivergence = divergence;
                            firstIdx = i;
                            secondIdx = j;
                        }
                    }
                }
                
                // Merge
                Cluster firstCluster = clusters.get(firstIdx); 
                Cluster secondCluster = clusters.get(secondIdx);
                firstCluster = firstCluster.merge(secondCluster);
                clusters.set(firstIdx, firstCluster);
                clusters.remove(secondIdx);
                
                // Initiate new cluster
                Cluster cluster = new Cluster(data[0], p);
                clusters.add(cluster);
            }
        }
        
        // Write result
        System.out.println("Writing clusters to text file...");
        try (Writer bw = new BufferedWriter(new FileWriter(CLUSTER_FILE_NAME))) {
            for(Cluster c : clusters) {
                bw.write(c.toString());
                bw.write("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public static void main(String[] args) throws Exception
    {
        ClusterBuilder cb = new ClusterBuilder();
        
//        // First run
//        cb.getNewsArticles();
//        cb.createCorpusArff();
//        
//        // Second run
//        cb.calculateWordFrequency();
//        
//        // Third run
//        cb.calculateMI();
//        
//        // Fourth run
//        cb.buildCluster();
    }
}
