package conceptprobability.initiator;

import conceptprobability.Cluster;

/**
 *
 * @author TOSHIBA PC
 */
public class KLDivergence
{
    public static int CORPUS_WORD_SIZE = 336945;
    public static int NUM_CATEGORIES = 10;
    
    public static double computeJensenShannonKLD(Cluster p, Cluster q)
    {
        Cluster temp = new Cluster(p);
        temp = temp.merge(q);
        double div = p.getTotalOccurence() * computeKLD(p,temp) + q.getTotalOccurence() * computeKLD(q,temp);
        return div / CORPUS_WORD_SIZE;
    }
    
    public static double computeKLD(Cluster p, Cluster q)
    {
        double sum = 0;
        for (int i = 0; i < NUM_CATEGORIES; i++) {
            double pCondProb = (double) p.getOccurence(i) / p.getTotalOccurence();
            double qCondProb = (double) q.getOccurence(i) / q.getTotalOccurence();
            if (pCondProb == 0)
                pCondProb = 0.0000001;
            if (qCondProb == 0)
                qCondProb = 0.0000001;
            sum += pCondProb * Math.log10(pCondProb / qCondProb);
        }
        return sum;
    }
}
