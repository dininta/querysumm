package conceptprobability.initiator;

import static weka.core.Utils.log2;

/**
 *
 * @author TOSHIBA PC
 */
public class MICalculator
{
    /**
     * Calculate mutual information of a word in certain category
     * Ex: "ball" in "sport" category
     * @param p = {{N00, N01}, {N10, N11}}
     * N00 -> number of documents not containing "ball" in other categories
     * N01 -> number of documents not containing "ball" in sport category
     * N10 -> number of documents containing "ball" in other categories
     * N11 -> number of documents containing "ball" in sport categories
     */
    public static double computeMI(int[][] p, int numDocs)
    {
        double mi = 0;
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                double sumtmp = (p[i][0] + p[i][1]) * (p[0][j] + p[1][j]);
                if (sumtmp == 0) sumtmp = 0.000001;
                if (p[i][j] > 0)
                    mi += p[i][j] * log2((numDocs * p[i][j]) / (sumtmp));
                else
                    mi += p[i][j] * log2((numDocs * 0.000001) / (sumtmp));
            }
        }
        return mi / numDocs;
    }
    
    /**
     * Calculate maximum mutual information of a word in every category
     * @param p have the size of category numbers
     */
    public static double computeMaxMI(int[] p, int numDocsPerCat , int numDocs)
    {
        double max = 0;
        int numDocsContainWords = 0;
        for (int i = 0; i < p.length; i++) {
            numDocsContainWords += p[i];
        }
        
        for (int i = 0; i < p.length; i++) {
            int n11 = p[i];
            int n10 = numDocsContainWords - p[i];
            int n01 = numDocsPerCat - p[i];
            int n00 = numDocs - numDocsContainWords - n01;
            double mi = computeMI(new int[][] {{n00, n01}, {n10, n11}}, numDocs);
            if (mi > max)
                max = mi;
        }
        
        return max;
    }
}
