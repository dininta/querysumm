package conceptprobability.initiator;

import IndonesianNLP.IndonesianSentenceDetector;
import IndonesianNLP.IndonesianSentenceFormalization;
import IndonesianNLP.IndonesianSentenceTokenizer;
import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author TOSHIBA PC
 */
public class ConstantCalculator
{
    public static int calculateNumOfWordInCorpus() throws Exception
    {
        int count = 0;
        BufferedReader reader = new BufferedReader(new FileReader("resource/conceptprobability/CorpusProcessed.txt"));
        String line = null;
        while ((line = reader.readLine()) != null ) {
            String[] data = line.split("\t");
            for (int i = 1; i < data.length; i++) {
                count += Integer.valueOf(data[i]);
            }
        }
        
        return count;
    }
    
    public static void main(String[] args) throws Exception
    {
        System.out.println(calculateNumOfWordInCorpus());
    }
}
