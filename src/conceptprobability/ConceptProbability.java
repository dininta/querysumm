package conceptprobability;

import IndonesianNLP.IndonesianSentenceDetector;
import IndonesianNLP.IndonesianSentenceFormalization;
import IndonesianNLP.IndonesianSentenceTokenizer;
import IndonesianNLP.IndonesianStemmer;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import net.sf.javailp.Constraint;
import net.sf.javailp.Linear;
import net.sf.javailp.OptType;
import net.sf.javailp.Problem;
import net.sf.javailp.ResultImpl;
import net.sf.javailp.Solver;
import net.sf.javailp.SolverFactory;
import net.sf.javailp.SolverFactoryGLPK;
import querysumm.Knapsack;
import querysumm.Summarizer;
import utils.CSVUtils;

/**
 *
 * @author TOSHIBA PC
 */
public class ConceptProbability extends Summarizer
{
    private List<Sentence> sentences;
    private double[][] sentenceScore;
    private int articlesWordSize = 0;                       // Number of words in articles
    private Sentence querySentence;
    
    /* CONSTANT VARIABLE */
    private static final boolean STEMMING = false;
    private static final boolean STOPWORD_ELIMINATION = true;
    private static final int K = 8;
    private static final int CORPUS_DOC_SIZE = 3000;         // Number of documents in corpus
    private static final int CORPUS_WORD_SIZE = 793655;      // Number of words in corpus
    private static final String SENTENCE_SELECTION = "MMR";
    private static double SENTENCE_SIM_BOUND = 0.8;
    
    /**
     * Constructor
     */
    public ConceptProbability(List<String> articles, String query) throws Exception
    {
        super(articles, query);
        ClusterManager.initCluster();
    }

    /**
     * Build the summary
     */
    public String buildSummary() throws Exception
    {
        long lStartTime, lEndTime;
        
        // Pre process
        System.out.println("\nPreprocessing...");
        lStartTime = System.nanoTime();
        preProcess();
        lEndTime = System.nanoTime();
        System.out.println("\nDone preprcessing, num of sentences: " + sentences.size());
        System.out.println("Elapsed time in milliseconds (preprocess): " + (lEndTime - lStartTime) / 1000000);
        
        // Delete redundancy if using DP2
        if (SENTENCE_SELECTION.equalsIgnoreCase("DP2")) {
            List<Integer> deletedIdx = new ArrayList<>();
            for (int i = 0; i < sentences.size()-1; i++) {
                for (int j = i+1; j < sentences.size(); j++) {
                    if (cosineSimilarity(i,j) > SENTENCE_SIM_BOUND) {
                        deletedIdx.add(i);
                        j = sentences.size();
                    }
                }
            }
            int[] array = new int[deletedIdx.size()];
            for (int i = 0; i < deletedIdx.size(); i++) array[i] = deletedIdx.get(i);
            for (int i = array.length-1; i >= 0; i--) {
                sentences.remove(array[i]);
            }
        }
        
        // Sentence scoring (QIScore)
        System.out.println("\nCalculating all sentence score...");
        sentenceScore = new double[sentences.size()][2];
        lStartTime = System.nanoTime();
        for (int i = 0; i < sentences.size(); i++) {
            sentenceScore[i][0] = QIScore(i);
        }
        lEndTime = System.nanoTime();
        System.out.println("Elapsed time in milliseconds (QIScore): " + (lEndTime - lStartTime) / 1000000);
        
        // Sentence scoring (QFocus)
        lStartTime = System.nanoTime();
        for (int i = 0; i < sentences.size(); i++) {
            sentenceScore[i][1] = QFocus(i);
        }
        lEndTime = System.nanoTime();
        System.out.println("Elapsed time in milliseconds (QFocus): " + (lEndTime - lStartTime) / 1000000);
                
        // Print result
        String csvFile = "result.csv";
        FileWriter writer = new FileWriter(csvFile);
        CSVUtils.writeLine(writer, Arrays.asList("Indeks", "QIScore", "QFocus", "Banyak kata", "Kalimat"));
        for (int i = 0; i < sentences.size(); i++) {
            CSVUtils.writeLine(writer, Arrays.asList(
                    Integer.toString(i),
                    Double.toString(sentenceScore[i][0]),
                    Double.toString(sentenceScore[i][1]),
                    Integer.toString(sentences.get(i).getLength()),
                    sentences.get(i).getText()
            ));
        }
        writer.flush();
        writer.close();
        
        // Sentence selection
        System.out.println("\nSentence selection...");
        lStartTime = System.nanoTime();
        List<Sentence> summary;
        if (SENTENCE_SELECTION.equalsIgnoreCase("MMR")) {
            summary = MMR();
        } else if (SENTENCE_SELECTION.equalsIgnoreCase("ILP")) {
            summary = ILP();
        } else {
            summary = DP();
        }
        lEndTime = System.nanoTime();
        System.out.println("Elapsed time in milliseconds: " + (lEndTime - lStartTime) / 1000000);
        
        // Return summary
        StringBuilder sb = new StringBuilder();
        for (Sentence s : summary) {
            sb.append(s.getText()).append("\n");
        }
        return sb.toString();
    }

    /**
     * Pre process
     */
    private void preProcess() throws Exception
    {
        this.sentences = new ArrayList<Sentence>();
        IndonesianSentenceDetector stDetector = new IndonesianSentenceDetector();
        IndonesianSentenceFormalization formalizer = new IndonesianSentenceFormalization();
        IndonesianSentenceTokenizer tokenizer = new IndonesianSentenceTokenizer();
        IndonesianStemmer stemmer = new IndonesianStemmer();
        formalizer.initStopword();
        
        for (String article : this.articles) {
            // Sentence delimitter
            List<String> rawSentences = stDetector.splitSentence(article);
            
            for (String sentence : rawSentences) {
                // Tokenization
                List<String> tokens = tokenizer.tokenizeSentence(sentence);
                // Delete stop words
                if (this.STOPWORD_ELIMINATION) {
                    String finalize = formalizer.deleteStopword(String.join(" ", tokens));
                    tokens.clear();
                    tokens = tokenizer.tokenizeSentence(finalize);
                }
                // Delete tokens which are not a word
                Iterator<String> it = tokens.iterator();
                while (it.hasNext()) {
                    String token = it.next();
                    if (!token.matches("[a-zA-Z]+")) it.remove();
                }
                // Stemming
                if (this.STEMMING) {
                    for (int j = 0; j < tokens.size(); j++) {
                        tokens.set(j, stemmer.stem(tokens.get(j)));
                    }
                }
                
                Sentence s = new Sentence(sentence, tokens.toArray(new String[tokens.size()]));
                sentences.add(s);
                this.articlesWordSize += tokens.size();
            }
        }
        
        // Pre process query
        List<String> tokens = tokenizer.tokenizeSentence(this.query);
        if (this.STOPWORD_ELIMINATION) {
            String finalize = formalizer.deleteStopword(String.join(" ", tokens));
            tokens.clear();
            tokens = tokenizer.tokenizeSentence(finalize);
        }
        Iterator<String> it = tokens.iterator();
        while (it.hasNext()) {
            String token = it.next();
            if (!token.matches("[a-zA-Z]+")) it.remove();
        }
        if (this.STEMMING) {
            for (String word : tokens) {
                word = stemmer.stem(word);
            }
        }
        this.querySentence = new Sentence(this.query, tokens.toArray(new String[tokens.size()]));
    }
    
    /**
     * Pre process
     */
    private void preProcess2() throws Exception
    {
        this.sentences = new ArrayList<Sentence>();
        IndonesianSentenceDetector stDetector = new IndonesianSentenceDetector();
        IndonesianSentenceFormalization formalizer = new IndonesianSentenceFormalization();
        IndonesianSentenceTokenizer tokenizer = new IndonesianSentenceTokenizer();
        IndonesianStemmer stemmer = new IndonesianStemmer();
        formalizer.initStopword();
        
        List<List<String>> rawSentences = new ArrayList<>();
        for (String article : this.articles) {
            // Sentence delimitter
            rawSentences.add(stDetector.splitSentence(article));
        }
        int i = 0;
        while (!rawSentences.isEmpty()) {
            if (!rawSentences.get(i).isEmpty()) {
                String sentence = rawSentences.get(i).get(0);
                // Tokenization
                List<String> tokens = tokenizer.tokenizeSentence(sentence);
                // Delete stop words
                if (this.STOPWORD_ELIMINATION) {
                    String finalize = formalizer.deleteStopword(String.join(" ", tokens));
                    tokens.clear();
                    tokens = tokenizer.tokenizeSentence(finalize);
                }
                // Delete tokens which are not a word
                Iterator<String> it = tokens.iterator();
                while (it.hasNext()) {
                    String token = it.next();
                    if (!token.matches("[a-zA-Z]+")) it.remove();
                }
                // Stemming
                if (this.STEMMING) {
                    for (int j = 0; j < tokens.size(); j++) {
                        tokens.set(j, stemmer.stem(tokens.get(j)));
                    }
                }
                
                Sentence s = new Sentence(sentence, tokens.toArray(new String[tokens.size()]));
                sentences.add(s);
                this.articlesWordSize += tokens.size();
                rawSentences.get(i).remove(sentence);
                i = (i+1) % rawSentences.size();
            } else {
                List<String> temp = rawSentences.get(i);
                rawSentences.remove(temp);
                if (i >= rawSentences.size()) i = 0;
            }
        }
        
        // Pre process query
        List<String> tokens = tokenizer.tokenizeSentence(this.query);
        if (this.STOPWORD_ELIMINATION) {
            String finalize = formalizer.deleteStopword(String.join(" ", tokens));
            tokens.clear();
            tokens = tokenizer.tokenizeSentence(finalize);
        }
        Iterator<String> it = tokens.iterator();
        while (it.hasNext()) {
            String token = it.next();
            if (!token.matches("[a-zA-Z]+")) it.remove();
        }
        if (this.STEMMING) {
            for (String word : tokens) {
                word = stemmer.stem(word);
            }
        }
        this.querySentence = new Sentence(this.query, tokens.toArray(new String[tokens.size()]));
    }

    /**
     * Used in sentence selection to calculate similarity with selected sentence
     */
    private double cosineSimilarity(int i, int j)
    {
        HashSet<String> words = new HashSet<>();
        for (String w : sentences.get(i).getWords()) {
            words.add(w);
        }
        for (String w : sentences.get(j).getWords()) {
            words.add(w);
        }
        
        double[] docVector1 = new double[words.size()], docVector2 = new double[words.size()];
        int idx = 0;
        for (String word : words) {
            docVector1[idx] = 0;
            for (String w : sentences.get(i).getWords()) {
                if (w.equalsIgnoreCase(word)) docVector1[idx]++;
            }
            docVector2[idx] = 0;
            for (String w : sentences.get(j).getWords()) {
                if (w.equalsIgnoreCase(word)) docVector2[idx]++;
            }
            idx++;
        }
        
        double dotProduct = 0.0;
        double magnitude1 = 0.0;
        double magnitude2 = 0.0;
        double cosineSimilarity = 0.0;

        for (int k = 0; k < docVector1.length; k++) {
            dotProduct += docVector1[k] * docVector2[k];  //a.b
            magnitude1 += Math.pow(docVector1[k], 2);  //(a^2)
            magnitude2 += Math.pow(docVector2[k], 2); //(b^2)
        }

        magnitude1 = Math.sqrt(magnitude1);//sqrt(a^2)
        magnitude2 = Math.sqrt(magnitude2);//sqrt(b^2)

        if (magnitude1 != 0.0 | magnitude2 != 0.0) {
            cosineSimilarity = dotProduct / (magnitude1 * magnitude2);
        } else {
            return 0.0;
        }
        return cosineSimilarity;
    }

    /**
     * Calculate the representation score of a sentence
     * The more representative the sentence of whole documents, the higher score it gets
     */
    private double QIScore(int i)
    {
        String[] words = sentences.get(i).getWords();
        double docProb = (double) articles.size() / (articles.size() + CORPUS_DOC_SIZE);
        double corpusProb = 1 - docProb;
        
        for (String word : words) {
            Cluster c = ClusterManager.getCluster(word);
            if (c == null) {
                docProb *= (double) 1 / this.articlesWordSize;
                corpusProb *= (double) 1 / CORPUS_WORD_SIZE;
            } else {
                docProb *= (double) ClusterManager.countOccurence(c, this.sentences) / this.articlesWordSize;
                corpusProb *= (double) c.getTotalOccurence() / CORPUS_WORD_SIZE;
            }
        }
        
        return docProb / (docProb + corpusProb);
    }
    
    /**
     * Calculate the dependency between two words
     * w1 is word from query
     * w2 is word from articles
     */
    private double pHAL(String w1, String w2)
    {
        if (w1.equalsIgnoreCase(w2)) return 1.0;
        
        // Calculate occurence & co-occurence
        int countOccurence = 0;
        int countCoOccurence[] = new int[K];
        for (int i = 0; i < K; i++) {
            countCoOccurence[i] = 0;
        }
        List<String> words = new ArrayList<>();
        for (Sentence s : sentences) {
            words.addAll(Arrays.asList(s.getWords()));
        }
        for (int i = 0; i < words.size(); i++) {
            if (words.get(i).equalsIgnoreCase(w2)) {
                countOccurence++;
                int idx = i - 1;
                for (int j = 0; j < K; j++) {
                    if (idx < 0) break;
                    if (words.get(idx).equalsIgnoreCase(w1))
                        countCoOccurence[j]++;
                    idx--;
                }
                idx = i + 1;
                for (int j = 0; j < K; j++) {
                    if (idx >= words.size()) break;
                    if (words.get(idx).equalsIgnoreCase(w1))
                        countCoOccurence[j]++;
                    idx++;
                }
            }
        }
        
        // Calculate pHAL
        int sum = 0;
        for (int i = 0; i < K; i++) {
            sum += (K - i) * countCoOccurence[i];
        }
        if (sum == 0)
            sum = 1;
        return (double) sum / (countOccurence * K * K * (K+1));
    }
    
    /**
     * Calculate the query-relevant score of a sentence
     */
    private double QFocus(int i)
    {
        String[] words = this.sentences.get(i).getWords();
        String[] queryWords = this.querySentence.getWords();
//        double score = 1;
//        
//        for (String word : words) {
//            Cluster c = ClusterManager.getCluster(word);
//            if (c == null) {
//                score *= (double) 1 / words.length;
//            } else {
//                score *= (double) ClusterManager.countOccurence(c, sentences.get(i)) / words.length;
//            }
//            double sum = 0;
//            for (String queryWord : queryWords) {
//                Cluster cq = ClusterManager.getCluster(queryWord);
//                if (cq == null) {
//                    sum += pHAL(queryWord, word) * (double) 1 / this.articlesWordSize;
//                } else {
//                    sum += pHAL(queryWord, word) * (double) ClusterManager.countOccurence(cq, sentences) / this.articlesWordSize;
//                }
//            }
//            score *= sum;
//        }
//        
//        return score;
        
        double sim = 0;
        for (String word : words) {
            for (String queryWord : queryWords) {
                sim += pHAL(queryWord, word);
            }
        }
        return (double) sim / (sentences.get(i).getLength() * querySentence.getLength());
    }
    
    /**
     * Sentence selection using MMR
     */
    private List<Sentence> MMR()
    {
        List<Sentence> summary = new ArrayList<>();
        List<Integer> selectedIdx = new ArrayList<>();
        int length = 0;
        
        while (selectedIdx.size() != sentences.size()) {
            double max = 0;
            int idxMax = 0;
            for (int i = 0; i < sentences.size(); i++) {
                if (!selectedIdx.contains(i)) {
                    double penalty = 0;
                    for (int idx : selectedIdx) {
                        double sim = cosineSimilarity(i, idx);
                        if (penalty < sim)
                            penalty = sim;
                    }
                    double score = LAMBDA * sentenceScore[i][0] + (1 - LAMBDA) * sentenceScore[i][1] - penalty;
                    if (score > max) {
                        max = score;
                        idxMax = i;
                    }
                }
            }
            length += sentences.get(idxMax).getLength();
            if (length <= SUMMARY_LENGTH) {
                summary.add(sentences.get(idxMax));
                System.out.println("Selected sentence's index: " + idxMax);
                selectedIdx.add(idxMax);
            } else {
                break;
            }
        }
        return summary;
    }

    /**
     * Sentence selection using ILP
     */
    private List<Sentence> ILP()
    {
        int numSentences = this.sentences.size();
        
        SolverFactory factory = new SolverFactoryGLPK();
        factory.setParameter(Solver.VERBOSE, 0);
        Problem problem = new Problem();
        
        // Objective function
        Linear linear = new Linear();
        for (int i=0; i < numSentences; i++) {
            String varName = "x".concat("_").concat(String.valueOf(i));
            linear.add(LAMBDA * sentenceScore[i][0] + (1 - LAMBDA) * sentenceScore[i][1], varName);
        }
        for (int i=0; i < numSentences-1; i++) {
            for (int j=i+1; j < numSentences; j++) {
                String varName = "x".concat("_").concat(String.valueOf(i)).concat("_").concat(String.valueOf(j));
                linear.add(-1 * cosineSimilarity(i, j), varName);
            }
        }
        problem.setObjective(linear, OptType.MAX);
        
        // Constraint panjang ringkasan
        int counter = 0;
        linear = new Linear();
        for (int i=0; i < numSentences; i++) {
            String varName = "x".concat("_").concat(String.valueOf(i));
            linear.add(sentences.get(i).getLength(), varName);
        }
        problem.add(new Constraint(String.valueOf(counter), linear, "<=", SUMMARY_LENGTH));
        counter++;

        // Constraint lainnya
        for (int i=0; i < numSentences-1; i++) {
            for (int j=i+1; j < numSentences; j++) {
                // Constraint aij - ai <= 0
                linear = new Linear();
                linear.add(1, "x".concat("_").concat(String.valueOf(i)).concat("_").concat(String.valueOf(j)));
                linear.add(-1, "x".concat("_").concat(String.valueOf(i)));
                problem.add(new Constraint(String.valueOf(counter), linear, "<=", 0));
                counter++;
                
                // Constraint aij - aj <= 0
                linear = new Linear();
                linear.add(1, "x".concat("_").concat(String.valueOf(i)).concat("_").concat(String.valueOf(j)));
                linear.add(-1, "x".concat("_").concat(String.valueOf(j)));
                problem.add(new Constraint(String.valueOf(counter), linear, "<=", 0));
                counter++;
                
                // Constraint ai + aj - aij <= 1
                linear = new Linear();
                linear.add(-1, "x".concat("_").concat(String.valueOf(i)).concat("_").concat(String.valueOf(j)));
                linear.add(1, "x".concat("_").concat(String.valueOf(i)));
                linear.add(1, "x".concat("_").concat(String.valueOf(j)));
                problem.add(new Constraint(String.valueOf(counter), linear, "<=", 1));
                counter++;
            }
        }
        
        // 0 <= x <= 1, x adalah integer
        for (int i=0; i < numSentences; i++) {
            linear = new Linear();
            String varName = "x".concat("_").concat(String.valueOf(i));
            linear.add(1, varName);
            problem.add(new Constraint(String.valueOf(counter), linear, ">=", 0));
            counter++;
            problem.add(new Constraint(String.valueOf(counter), linear, "<=", 1));
            counter++;
            
            problem.setVarType(varName, Integer.class);
        }
        for (int i=0; i < numSentences-1; i++) {
            for (int j=i+1; j < numSentences; j++) {
                linear = new Linear();
                String varName = "x".concat("_").concat(String.valueOf(i)).concat("_").concat(String.valueOf(j));
                linear.add(1, varName);
                problem.add(new Constraint(String.valueOf(counter), linear, ">=", 0));
                counter++;
                problem.add(new Constraint(String.valueOf(counter), linear, "<=", 1));
                counter++;
            
                problem.setVarType(varName, Integer.class);
            }
        }

        System.out.println("Solving ILP problem...");
        Solver solver = factory.get();
        ResultImpl result =  (ResultImpl) solver.solve(problem);
        
        List<Sentence> summary = new ArrayList<>();
        System.out.println("Selected sentence:");
        for (int i = 0; i < numSentences; i++) {
            if (result.get("x".concat("_").concat(String.valueOf(i))).intValue() == 1) {
                System.out.println(i);
                summary.add(sentences.get(i));
            }
        }
        return summary;
    }

    /**
     * Sentence selection using DP
     */
    private List<Sentence> DP()
    {
        int sentenceWeight[] = new int[this.sentences.size()];
        for (int i = 0; i < this.sentences.size(); i++) {
            sentenceWeight[i] = this.sentences.get(i).getLength();
        }
        double finalScore[] = new double[this.sentences.size()];
        for (int i = 0; i < this.sentences.size(); i++) {
            finalScore[i] = LAMBDA * sentenceScore[i][0] + (1 - LAMBDA) * sentenceScore[i][1];
        }
        
        Knapsack problem = new Knapsack(SUMMARY_LENGTH, sentenceWeight, finalScore);
        problem.solve();
        List<Integer> selectedIdx = problem.getSelectedItem();
        List<Sentence> summary = new ArrayList<>();
        for (int i : selectedIdx) {
            System.out.println("Selected sentence's index: " + (i-1));
            summary.add(sentences.get(i-1));
        }
        
        return summary;
    }
}
