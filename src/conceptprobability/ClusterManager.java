package conceptprobability;

import IndonesianNLP.IndonesianSentenceTokenizer;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author TOSHIBA PC
 */
public class ClusterManager
{
    private static List<Cluster> clusters;
    private static IndonesianSentenceTokenizer tokenizer = new IndonesianSentenceTokenizer();
    
    /* Constant variable */
    private static final String CLUSTER_FILE_NAME = "resource/conceptprobability/Cluster.txt";
    private static final int NUM_OF_CATEGORY = 10;
    
    public static void initCluster() throws Exception
    {
        System.out.println("Reading cluster data from file...");
        clusters = new ArrayList<Cluster>();
        BufferedReader reader = new BufferedReader(new FileReader(CLUSTER_FILE_NAME));
        String line = null;
        while ((line = reader.readLine()) != null ) {
            String[] data = line.split("\t");
            int[] p = new int[NUM_OF_CATEGORY];
            int i = 0;
            for (i = 0; i < NUM_OF_CATEGORY; i++) {
                p[i] = Integer.valueOf(data[i]);
            }
            String[] words = new String[data.length - NUM_OF_CATEGORY];
            for (int j = 0; j < words.length; j++) {
                words[j] = data[i];
                i++;
            }
            clusters.add(new Cluster(words, p));
        }
        reader.close();
    }
    
    public static Cluster getCluster(String word)
    {
        for (Cluster c : clusters) {
            if (c.contains(word)) {
                return c;
            }
        }
        return null;
    }
    
    public static int countOccurence(Cluster c, List<Sentence> sentences)
    {
        int count = 0;
        for (Sentence s : sentences) {
            String[] words = s.getWords();
            for (String w : words) {
                if (c.contains(w)) {
                    count++;
                }
            }
        }
        return count;
    }
    
    public static int countOccurence(Cluster c, Sentence s)
    {
        int count = 0;
        String[] words = s.getWords();
        for (String w : words) {
            if (c.contains(w)) {
                count++;
            }
        }
        return count;
    }
}
