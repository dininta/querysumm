package conceptprobability;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author TOSHIBA PC
 */
public class Cluster
{
    private Set<String> words;
    private int[] numOccurence; // length equals to NUM_CATEGORIES
    
    public Cluster(String w, int[] p)
    {
        this.words = new HashSet<>();
        this.words.add(w);
        
        this.numOccurence = new int[p.length];
        for (int i = 0; i < p.length; i++) {
            this.numOccurence[i] = p[i];
        }
    }
    public Cluster(String[] words, int[] p)
    {
        this.words = new HashSet<>();
        for (String w : words) {
            this.words.add(w);
        }
        
        this.numOccurence = new int[p.length];
        for (int i = 0; i < p.length; i++) {
            this.numOccurence[i] = p[i];
        }
    }
    public Cluster(Cluster c)
    {
        this.words = new HashSet<>();
        Set<String> words = c.getWords();
        for (String w : words) {
            this.words.add(w);
        }
        this.numOccurence = new int[c.getNumOccurenceSize()];
        for (int i = 0; i < this.numOccurence.length; i++) {
            this.numOccurence[i] += c.getOccurence(i);
        }
    }
    public Set<String> getWords()
    {
        return this.words;
    }
    public int getOccurence(int idx)
    {
        return numOccurence[idx];
    }
    public int getNumOccurenceSize()
    {
        return numOccurence.length;
    }
    public int getTotalOccurence()
    {
        int sum = 0;
        for (int i : this.numOccurence) {
            sum += i;
        }
        return sum;
    }
    public boolean contains(String w)
    {
        return this.words.contains(w);
    }
    public Cluster merge(Cluster c)
    {
        Set<String> words = c.getWords();
        for (String w : words) {
            this.words.add(w);
        }
        for (int i = 0; i < this.numOccurence.length; i++) {
            this.numOccurence[i] += c.getOccurence(i);
        }
        return this;
    }
    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        for (int i : this.numOccurence) {
            sb.append(i).append("\t");
        }
        for (String w : this.words) {
            sb.append(w).append("\t");
        }
        return sb.toString();
    }
}
