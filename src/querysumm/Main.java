package querysumm;

import conceptlink.ConceptLink;
import conceptprobability.ConceptProbability;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author TOSHIBA PC
 */
public class Main
{
    public static String QUERY = "Pusat dan kekuatan gempa dieng";
    public static String FOLDER_NAME = "data/gempa-dieng";
    public static String FILE_RESULT_NAME = "../rouge2.0-0.2-distribute/nus-modif-eksperimen/system/g-gempa-dieng200_CLEN1D2.txt";
    
    /**
     * Convert text file to string
     */
    public static String readFile(File file)
    {
        try {
            InputStream is = new FileInputStream(file);
            BufferedReader buf = new BufferedReader(new InputStreamReader(is));
            String line = buf.readLine();
            StringBuilder sb = new StringBuilder();
            while (line != null) {
                sb.append(line).append(" ");
                line = buf.readLine();
            }
            return sb.toString();
        } catch (Exception e) {}
        return null;
    }
    
    /**
     * Convert string to text file
     */
    public static void writeFile(String text) throws Exception
    {
        File file = new File(FILE_RESULT_NAME);
        if (!file.exists()) {
            file.createNewFile();
        }
        java.io.FileWriter fw = new java.io.FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(text);
        bw.close();
    }

    /**
     * Main program
     */
    public static void main(String[] args) throws Exception
    {
        // Get input from user
//        Scanner input = new Scanner(System.in);
//        System.out.print("Folder name: ");
//        FOLDER_NAME = input.nextLine();
//        System.out.print("Query: ");
//        QUERY = input.nextLine();
//        System.out.print("File result name: ");
//        FILE_RESULT_NAME = input.nextLine().concat(".txt");

        // Get news articles
        System.out.println("Getting news articles...");
        List<String> articles = new ArrayList<String>();
        File folder = new File(FOLDER_NAME);
        File[] files = folder.listFiles();
        for (File file : files) {
            if (file.isFile()) {
                articles.add(readFile(file));
            }
        }
        System.out.println("Num of articles: " + articles.size());
        
        Summarizer summarizer = new ConceptLink(articles, QUERY);
        summarizer.setLambda(0.8);
        String summary = summarizer.buildSummary();

        System.out.println("Writing summary to text file...");
        writeFile(summary);
    }
}
