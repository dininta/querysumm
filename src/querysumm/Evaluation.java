package querysumm;

import conceptlink.ConceptLink;
import conceptprobability.ConceptProbability;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author TOSHIBA PC
 */
public class Evaluation
{
    public static String[] QUERIES = {
        "Penyebab penahanan Ivan Haz",
        "Kasus penipuan Ivan Haz",
        "Penyebab pesawat salah mendarat dan apa yang dilakukan petugas setelah kesalahan tersebut",
        "Penyebab pesawat salah mendarat dan dampak yang dapat ditimbulkannya",
        "Latar belakang gedung yang ambruk di Bintaro serta kronologi dan penyebab kejadian",
        "Kronologi robohnya gedung di Bintaro",
        "Bentuk dan manfaat dari kemitraan Indosat dan Fujitsu",
        "Kronologi kasus bunuh diri",
        "Pusat dan kekuatan gempa dieng",
        "Kondisi setelah gempa"
    };
    public static String[] FOLDER_NAMES = {
        "data/kasus-ivan-haz",
        "data/kasus-ivan-haz",
        "data/salah-darat",
        "data/salah-darat",
        "data/gedung-roboh",
        "data/gedung-roboh",
        "data/indo-fuji",
        "data/bunuh-diri",
        "data/gempa-dieng",
        "data/gempa-dieng-b"
    };
    public static String[] FILE_NAMES = {
        "a-ivan-haz-penyebab",
        "a-ivan-haz-penipuan",
        "b-salah-darat",
        "b-salah-darat-akibat",
        "c-gedung-roboh",
        "c-gedung-roboh-kronologi",
        "d-indo-fuji",
        "f-bunuh-diri",
        "g-gempa-dieng",
        "g-gempa-dieng-b"
    };
    public static String FOLDER_RESULT_NAME = "../rouge2.0-0.2-distribute/nus-modif-pengujian/system/";
    
    /**
     * Convert text file to string
     */
    public static String readFile(File file)
    {
        try {
            InputStream is = new FileInputStream(file);
            BufferedReader buf = new BufferedReader(new InputStreamReader(is));
            String line = buf.readLine();
            StringBuilder sb = new StringBuilder();
            while (line != null) {
                sb.append(line).append(" ");
                line = buf.readLine();
            }
            return sb.toString();
        } catch (Exception e) {}
        return null;
    }
    
    /**
     * Convert string to text file
     */
    public static void writeFile(String text, String filename) throws Exception
    {
        File file = new File(filename);
        if (!file.exists()) {
            file.createNewFile();
        }
        java.io.FileWriter fw = new java.io.FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(text);
        bw.close();
    }

    /**
     * Main program
     */
    public static void main(String[] args) throws Exception
    {
        for (int i = 0; i < FOLDER_NAMES.length; i++) {
            // Get news articles
            System.out.println("Getting news articles...");
            List<String> articles = new ArrayList<String>();
            File folder = new File(FOLDER_NAMES[i]);
            File[] files = folder.listFiles();
            for (File file : files) {
                if (file.isFile()) {
                    articles.add(readFile(file));
                }
            }
            System.out.println("Num of articles: " + articles.size());

            // 200 kata
            Summarizer summarizer200 = new ConceptLink(articles, QUERIES[i]);
            summarizer200.setSummaryLength(200);
            summarizer200.setLambda(0.8);
            String summary200 = summarizer200.buildSummary();
            String filename200 = FOLDER_RESULT_NAME.concat(FILE_NAMES[i]).concat("200_CLEN1D2.txt");
            writeFile(summary200, filename200);
            
            // 100 kata
            Summarizer summarizer100 = new ConceptLink(articles, QUERIES[i]);
            summarizer100.setSummaryLength(100);
            summarizer100.setLambda(0.8);
            String summary100 = summarizer100.buildSummary();
            String filename100 = FOLDER_RESULT_NAME.concat(FILE_NAMES[i]).concat("100_CLEN1D2.txt");
            writeFile(summary100, filename100);
            
            // 250 kata
            Summarizer summarizer250 = new ConceptLink(articles, QUERIES[i]);
            summarizer250.setSummaryLength(250);
            summarizer250.setLambda(0.8);
            String summary250 = summarizer250.buildSummary();
            String filename250 = FOLDER_RESULT_NAME.concat(FILE_NAMES[i]).concat("250_CLEN1D2.txt");
            writeFile(summary250, filename250);
        }
    }
}
