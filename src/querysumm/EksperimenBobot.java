package querysumm;

import conceptlink.ConceptLink;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author TOSHIBA PC
 */
public class EksperimenBobot
{
    public static String QUERY = "Pusat dan kekuatan gempa dieng";
    public static String FOLDER_NAME = "data/gempa-dieng";
    public static String FOLDER_RESULT_NAME = "../rouge2.0-0.2-distribute/nus-modif-bobot/system/";
    
    /**
     * Convert text file to string
     */
    public static String readFile(File file)
    {
        try {
            InputStream is = new FileInputStream(file);
            BufferedReader buf = new BufferedReader(new InputStreamReader(is));
            String line = buf.readLine();
            StringBuilder sb = new StringBuilder();
            while (line != null) {
                sb.append(line).append(" ");
                line = buf.readLine();
            }
            return sb.toString();
        } catch (Exception e) {}
        return null;
    }
    
    /**
     * Convert string to text file
     */
    public static void writeFile(String text, String filename) throws Exception
    {
        File file = new File(filename);
        if (!file.exists()) {
            file.createNewFile();
        }
        java.io.FileWriter fw = new java.io.FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(text);
        bw.close();
    }

    /**
     * Main program
     */
    public static void main(String[] args) throws Exception
    {
        // Get news articles
        System.out.println("Getting news articles...");
        List<String> articles = new ArrayList<String>();
        File folder = new File(FOLDER_NAME);
        File[] files = folder.listFiles();
        for (File file : files) {
            if (file.isFile()) {
                articles.add(readFile(file));
            }
        }
        System.out.println("Num of articles: " + articles.size());
        
        // NUS 200 ringkasan
        for (int i = 1; i <= 9; i++) {
            Summarizer summarizer = new ConceptLink(articles, QUERY);
            summarizer.setLambda(0.1 * i);
            summarizer.setSummaryLength(200);
            String summary = summarizer.buildSummary();
            String filename = FOLDER_RESULT_NAME.concat("g-gempa-dieng200_CLEN1D2-").concat(Integer.toString(i)).concat(".txt");
            writeFile(summary, filename);
        }
        
        // NUS 100 ringkasan
        for (int i = 1; i <= 9; i++) {
            Summarizer summarizer = new ConceptLink(articles, QUERY);
            summarizer.setLambda(0.1 * i);
            summarizer.setSummaryLength(100);
            String summary = summarizer.buildSummary();
            String filename = FOLDER_RESULT_NAME.concat("g-gempa-dieng100_CLEN1D2-").concat(Integer.toString(i)).concat(".txt");
            writeFile(summary, filename);
        }
    }
}
