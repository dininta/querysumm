package querysumm;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author TOSHIBA PC
 */
public class Knapsack
{
    private int W;
    private int numItems;
    private int[] weightItems;
    private double[] valItems;
    private double[][] K;
    
    /**
     * Constructor
     */
    public Knapsack(int W, int[] weightItems, double[] valItems)
    {
        this.W = W;
        this.numItems = weightItems.length;
        this.weightItems = new int[this.numItems];
        for (int i = 0; i < this.numItems; i++) {
            this.weightItems[i] = weightItems[i];
        }
        this.valItems = new double[this.numItems];
        for (int i = 0; i < this.numItems; i++) {
            this.valItems[i] = valItems[i];
        }
    }
    
    private static double max(double a, double b)
    {
        return (a > b)? a : b;
    }
    
    /**
     * Solve knapsack problem using dynamic programming
     */
    public void solve()
    {
        int i, w;
        this.K = new double[this.numItems + 1][this.W + 1];
      
        // Build table K[][] in bottom up manner
        for (i = 0; i <= this.numItems; i++) {
            for (w = 0; w <= W; w++) {
                if (i == 0 || w == 0) {
                    K[i][w] = 0;
                } else if (this.weightItems[i-1] <= w) {
                    K[i][w] = max(valItems[i-1] + K[i-1][w - weightItems[i-1]],  K[i-1][w]);
                } else {
                    K[i][w] = K[i-1][w];
                }
            }
        }
    }
    
    public List<Integer> getSelectedItem()
    {
        return reconstruct(this.numItems, this.W);
    }
    
    /**
     * Reconstruct subset of items 1..i with weight <= w and value f[i][w]
     */
    private List<Integer> reconstruct(int i, int w)
    {
        if (i == 0) {
            List<Integer> result = new ArrayList<>();
            return result;
        }
        if (K[i][w] > K[i-1][w]) {
            // take item i
            List<Integer> result = reconstruct(i-1, w - this.weightItems[i-1]);
            result.add(i);
            return result;
        } else {
            // don't need item i
            return reconstruct(i-1, w);
        }
    }
    
    /**
     * Driver 
     */
    public static void main(String args[])
    {
        double val[] = new double[]{5, 2, 3};
        int wt[] = new int[]{5, 3, 4};
        int W = 9;
        
        Knapsack problem = new Knapsack(W, wt, val);
        problem.solve();
        List<Integer> selectedItem = problem.getSelectedItem();
        System.out.println(selectedItem);
    }
}
