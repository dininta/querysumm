package querysumm;

import java.util.List;

/**
 *
 * @author TOSHIBA PC
 */
public abstract class Summarizer
{
    protected List<String> articles;
    protected String query;
    protected static int SUMMARY_LENGTH = 200;
    protected static double LAMBDA = 0.8;
    
    /**
     * Constructor
     */
    public Summarizer(List<String> articles, String query)
    {
        this.articles = articles;
        this.query = query;
    }
    
    /**
     * Build the summary
     */
    public abstract String buildSummary() throws Exception;
    
    /**
     * Setter
     */
    public void setLambda(double lambda)
    {
        LAMBDA = lambda;
    }
    
    /**
     * Setter
     */
    public void setSummaryLength(int length)
    {
        SUMMARY_LENGTH = length;
    }
}
