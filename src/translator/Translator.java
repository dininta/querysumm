package translator;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;

/**
 *
 * @author TOSHIBA PC
 */
public class Translator
{
    private static final String DICTIONARY_FILENAME = "dict/MASTER";
    private static final LinkedHashMap<String, ArrayList<String>> dicts = new LinkedHashMap<>();
    
    /**
     * Initialize dictionary
     */
    private static void initDict() throws FileNotFoundException
    {
        BufferedReader fileReader = new BufferedReader(new FileReader(DICTIONARY_FILENAME));
        String line;

        try {
            String word = "";
            while ((line = fileReader.readLine()) != null) {
                if (!line.equals("")) {
                    if (line.startsWith("%1")) {
                        word = line.substring(2, line.length());
                    }
                    if (line.startsWith("%4")) {
                        String trans = line.substring(2, line.length());
                        addToDictionary(word, trans);
                    }
                    //derivation
                    if (line.startsWith("&") && line.substring(3, 5).equals("11") ) {
                        word = line.substring(5, line.length());
                    }
                    if (line.startsWith("#") && line.substring(3, 5).equals("00") ) {
                        String trans = line.substring(5, line.length());
                        addToDictionary(word, trans);
                    }
                } 
            }
        } catch (IOException ex) {
            Logger.getLogger(translator.Translator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Add to hash map
     */
    private static void addToDictionary(String key, String word)
    {
        String trans = word.replaceAll("\\(.*\\)", "");
        trans = trans.replaceAll("to ", "");
        ArrayList<String> translations = new ArrayList<>(Arrays.asList(trans.trim().split(";")));
        for (int i = 0; i < translations.size(); i++) {
            translations.set(i, translations.get(i).trim());
        }

        if (dicts.containsKey(key)) {
            HashSet temp = new HashSet(dicts.get(key));
            temp.addAll(translations);
            translations = new ArrayList(temp);
        }
        dicts.put(key, translations);
    }

    /**
     * Get translation by Google translate
     */
    private static String callUrlAndParseResult(String langFrom, String langTo, String word) throws Exception
    {
        String url = "https://translate.googleapis.com/translate_a/single?"+
                        "client=gtx&"+
                        "sl=" + langFrom + 
                        "&tl=" + langTo + 
                        "&dt=t&q=" + URLEncoder.encode(word, "UTF-8");
  
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection(); 
        con.setRequestProperty("User-Agent", "Mozilla/5.0");

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        // Parse result
        JSONArray jsonArray = new JSONArray(response.toString());
        JSONArray jsonArray2 = (JSONArray) jsonArray.get(0);
        JSONArray jsonArray3 = (JSONArray) jsonArray2.get(0);
        return jsonArray3.get(0).toString();
    }
    
    /**
     * Translate the word
     */
    public static ArrayList<String> getTranslation(String word) throws Exception
    {
        if (dicts.isEmpty()) {
            initDict();
        }
        ArrayList<String> result = dicts.get(word);
        if (result == null) {
            result = new ArrayList<String>();
            try {
                String translate = callUrlAndParseResult("id", "en", word);
                result.add(translate);
            } catch(SocketTimeoutException e) {}
        }
        return result;
    }
}
