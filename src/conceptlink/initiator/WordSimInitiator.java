package conceptlink.initiator;

import conceptlink.Word;
import conceptlink.WordPair;
import conceptlink.wordnet.WordnetConnectorID;
import conceptlink.wordnet.WordnetConnectorEN;
import edu.smu.tspell.wordnet.SynsetType;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import translator.Translator;

/**
 *
 * @author TOSHIBA PC
 */
public class WordSimInitiator
{
    public static final String WORD_LIST_FILE_NAME = "resource/WordList.txt";
    public static final String WORD_SIMILARITY_FILE_NAME = "resource/WordSimilarity.EN";
    public static final String LANG = "EN";
    public static final double THETA =  0.2;
    
    public static String getLongestSubstr(String s1, String s2)
    {
        String[] token1 = s1.split("\\s+");
        String[] token2 = s2.split("\\s+");
        
        int Start = 0;
        int Max = 0;
        for (int i = 0; i < token1.length; i++) {
            for (int j = 0; j < token2.length; j++) {
                int x = 0;
                while (token1[i + x].equals(token2[j + x])) {
                    x++;
                    if (((i + x) >= token1.length) || ((j + x) >= token2.length)) break;
                }
                if (x > Max) {
                    Max = x;
                    Start = i;
                }
             }
        }
        return String.join(" ", Arrays.copyOfRange(token1, Start, Start + Max));
    }
    
    public static String replace(String sentence, String text)
    {
        String[] tokenSt = sentence.split("\\s+");
        String[] tokenText = text.split("\\s+");
        String[] result = new String[tokenSt.length - tokenText.length];
        
        for (int i = 0; i <= tokenSt.length - tokenText.length; i++) {
            int x = i, j = 0;
            while (j < tokenText.length) {
                if (tokenSt[x].equalsIgnoreCase(tokenText[j])) {
                    j++;
                    x++;
                } else break;
            }
            if (j == tokenText.length) {
                int idxResult = 0, idxSt = 0;
                while (idxResult < result.length) {
                    if (idxSt == i) {
                        idxSt += tokenText.length;
                    }
                    result[idxResult] = tokenSt[idxSt];
                    idxResult++;
                    idxSt++;
                }
                return String.join(" ", result);
            }
        }
        return null;
    }

    public static List<String> getStringOverlap(String sentence1, String sentence2)
    {
        List<String> overlap = new ArrayList<String>();
        String s1 = sentence1;
        String s2 = sentence2;
        String longestSubstr = getLongestSubstr(s1, s2);
        while (!longestSubstr.equals("")) {
            overlap.add(longestSubstr);
            s1 = replace(s1, longestSubstr);
            s2 = replace(s2, longestSubstr);
            longestSubstr = getLongestSubstr(s1, s2);
        }
        return overlap;
    }
    
    public static double wordSimilarity(Word w1, Word w2, String lang) throws Exception
    {
        HashSet<String> def1, def2;
        if (lang.contains("ID")) {
            def1 = WordnetConnectorID.getWordDefinition(w1);
            def2 = WordnetConnectorID.getWordDefinition(w2);
        } else {
            def1 = WordnetConnectorEN.getWordDefinition(w1);
            def2 = WordnetConnectorEN.getWordDefinition(w2);
        }
        
        if (def1 == null || def2 == null)
            return 0;
        
        double highestScore = 0;
        for (String d1 : def1) {
            for (String d2 : def2) {
                double score = 0;
                List<String> strOverlap = getStringOverlap(d1, d2);
                for (String str : strOverlap) {
                    score += Math.pow(str.length(), 2);
                }
                score = score / (d1.length() * d2.length());
                if (score >= 1) {
                    return score;
                }
                if (score > highestScore) {
                    highestScore = score;
                }
            }
        }
        return highestScore;
    }

    public void initWordSim() throws IOException, Exception
    {
        if (LANG == "ID")
            WordnetConnectorID.initDb();
        List<String[]> words = new ArrayList<>();
        
        // Open file
        BufferedReader fileReader = new BufferedReader(new FileReader(WORD_LIST_FILE_NAME));
        String line;
        while ((line = fileReader.readLine()) != null) {
            String[] word = line.split("\t");
            if (word.length == 3) {
                words.add(word);
            }
        }
        System.out.println("Jumlah kata: " + words.size());
        
        // Calculate Wordsim
        for (int i = 0; i < words.size()-1; i++) {
            for (int j = i+1; j < words.size(); j++) {
                Word w1 = new Word(words.get(i)[2], words.get(i)[1]);
                Word w2 = new Word(words.get(j)[2], words.get(j)[1]);
                double similarity = wordSimilarity(w1, w2, LANG);
                if (similarity > THETA && !words.get(i)[0].equalsIgnoreCase(words.get(j)[0])) {
                    System.out.println(words.get(i)[0] + "\t" + words.get(i)[1] + "\t" + words.get(j)[0] + "\t" + words.get(j)[1] + "\t" + similarity);
                }
            }
        }
    }

    public static void main(String[] args) throws Exception
    {
        WordSimInitiator wsi = new WordSimInitiator();
        wsi.initWordSim();
    }
}
