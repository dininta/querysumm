package conceptlink.initiator;

import IndonesianNLP.IndonesianSentenceDetector;
import IndonesianNLP.IndonesianSentenceFormalization;
import IndonesianNLP.IndonesianSentenceTokenizer;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.tokenizers.NGramTokenizer;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.StringToWordVector;

/**
 *
 * @author TOSHIBA PC
 */
public class TfIdf
{
    private static final String WORD_FREQUENCY_FILE_NAME = "resource/WordFrequency.txt";
    private static final String CORPUS_FILE_NAME = "resource/Corpus.arff";
    
    public static void createCorpusArff() throws IOException, ClassNotFoundException, SQLException
    {
        IndonesianSentenceDetector stDetector = new IndonesianSentenceDetector();
        IndonesianSentenceFormalization formalizer = new IndonesianSentenceFormalization();
        IndonesianSentenceTokenizer tokenizer = new IndonesianSentenceTokenizer();
        formalizer.initStopword();
        
        // Create corpus.arff
        File file = new File(CORPUS_FILE_NAME);
        if (!file.exists()) {
            file.createNewFile();
        }
        java.io.FileWriter fw = new java.io.FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write("@relation complaint\n\n");
        bw.write("@attribute text string\n");
        bw.write("\n@data\n\n");

        // Get from database
        Class.forName("com.mysql.jdbc.Driver");
        Connection connect = DriverManager.getConnection("jdbc:mysql://localhost:3306/newsaggregator?user=root&password=dininta");
        Statement statement = connect.createStatement();
        ResultSet resultSet = statement.executeQuery("select * from artikel");

        // Write to corpus.arff
        while (resultSet.next()) {
            String article = "";
            List<String> rawSentences = stDetector.splitSentence(resultSet.getString("FULL_TEXT"));
            for (String sentence : rawSentences) {
                List<String> tokens = tokenizer.tokenizeSentence(formalizer.normalizeSentence(sentence));
                String finalize = formalizer.deleteStopword(String.join(" ", tokens));
                tokens.clear();
                tokens = tokenizer.tokenizeSentence(finalize);
                StringBuilder sb = new StringBuilder();
                for (String token : tokens) {
                    if (token.matches("[a-zA-Z]+")) {
                        sb.append(token).append(" ");
                    }
                }
                article = article.concat(sb.toString()).concat(" ");
            }
            bw.write("'" + article + "'\n");
        }
        
        bw.close();
    }

    public static void calculateWordFrequency() throws FileNotFoundException, IOException, Exception
    {
        System.out.println("Reading corpus...");
        BufferedReader reader = new BufferedReader(new FileReader(CORPUS_FILE_NAME));
        Instances data = new Instances(reader);
        reader.close();
        System.out.println("Num of instances: " + data.numInstances());
        
        // Set the tokenizer
        NGramTokenizer tokenizer = new NGramTokenizer();
        tokenizer.setNGramMinSize(1);
        tokenizer.setNGramMaxSize(1);
        tokenizer.setDelimiters(" ");
        
        // Set the filter
        StringToWordVector filter = new StringToWordVector();
        filter.setAttributeIndicesArray(new int[]{0});
        filter.setOutputWordCounts(true);
        filter.setTokenizer(tokenizer);
        filter.setInputFormat(data);
        filter.setWordsToKeep(1000000);
        filter.setLowerCaseTokens(true);
        filter.setTFTransform(false);
        filter.setIDFTransform(false);

        // Filter the input instances into the output ones
        System.out.println("Filtering instances...");
        Instances outputInstances = Filter.useFilter(data, filter);
        System.out.println("Num of words: " + outputInstances.numAttributes());
        
//        ArffSaver saver = new ArffSaver();
//        saver.setInstances(outputInstances);
//        saver.setFile(new File("test.arff"));
//        saver.writeBatch();
        
        System.out.println("Calculating term frequencies...");
        int wordSize = 0;
        int[][] termFrequency = new int[outputInstances.numAttributes()][2];
        for (int i = 0; i < outputInstances.numAttributes(); i++) {
            termFrequency[i][0] = 0;
            termFrequency[i][1] = 0;
        }
        for (int i = 0; i < outputInstances.numInstances(); i++) {
            System.out.println("Instance " + i);
            for (int j = 0; j < outputInstances.numAttributes(); j++) {
                if (outputInstances.instance(i).value(j) != 0) {
                    termFrequency[j][0] += outputInstances.instance(i).value(j);
                    termFrequency[j][1] += 1;
                }
                wordSize += outputInstances.instance(i).value(j);
            }
        }
        
        // Write to Word Frequency file
        System.out.println("Writing to text file...");
        File file = new File(WORD_FREQUENCY_FILE_NAME);
        if (!file.exists()) {
            file.createNewFile();
        }
        java.io.FileWriter fw = new java.io.FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        for (int i = 0; i < outputInstances.numAttributes(); i++) {
            double tf = (double) termFrequency[i][0] / wordSize;
            double idf = Math.log10((double) outputInstances.numInstances() / termFrequency[i][1]);
            double val = tf*idf;
            bw.write(outputInstances.attribute(i).name() + "\t" + val + "\n");
        }
        bw.close();
    }

    public static void main(String[] args) throws Exception
    {
        TfIdf.calculateWordFrequency();
    }
}
