package conceptlink.initiator;

import IndonesianNLP.IndonesianPOSTagger;
import IndonesianNLP.IndonesianSentenceDetector;
import IndonesianNLP.IndonesianSentenceTokenizer;
import IndonesianNLP.IndonesianStemmer;
import conceptlink.Sentence;
import conceptlink.Word;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Writer;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author TOSHIBA PC
 */
public class WordList
{
    public static IndonesianSentenceDetector stDetector = new IndonesianSentenceDetector();
    public static IndonesianSentenceTokenizer tokenizer = new IndonesianSentenceTokenizer();
    public static IndonesianPOSTagger posTagger = new IndonesianPOSTagger();
    public static IndonesianStemmer stemmer = new IndonesianStemmer();
        
    public static String[] FOLDER_NAME = {
        "data/bunuh-diri",
        "data/gedung-roboh",
        "data/gempa-dieng",
        "data/gempa-dieng-b",
        "data/indo-fuji",
        "data/kasus-ivan-haz",
        "data/salah-darat"
    };
    
    public static String readFile(File file)
    {
        try {
            InputStream is = new FileInputStream(file);
            BufferedReader buf = new BufferedReader(new InputStreamReader(is));
            String line = buf.readLine();
            StringBuilder sb = new StringBuilder();
            while (line != null) {
                sb.append(line).append(" ");
                line = buf.readLine();
            }
            return sb.toString();
        } catch (Exception e) {}
        return null;
    }
    
    public static Set<Word> processArticle(String article) throws Exception
    {
        Set<Word> result = new HashSet<>();
        List<String> rawSentences = stDetector.splitSentence(article);
        for (String sentence : rawSentences) {
            List<String> tokens = tokenizer.tokenizeSentence(sentence);
            List<String[]> posTagged = posTagger.doPOSTag(String.join(" ", tokens));
            List<Word> concepts = new Sentence(sentence, posTagged).getConcepts();
            for (Word w : concepts) {
                result.add(w);
                result.add(new Word(stemmer.stem(w.getText()), w.getSynsetType().toString()));
            }
        }
        return result;
    }
    
    public static void main(String[] args) throws Exception
    {
        Set<Word> words = new HashSet<Word>();
        
        for (String folderName : FOLDER_NAME) {
            File folder = new File(folderName);
            File[] files = folder.listFiles();
            for (File file : files) {
                if (file.isFile()) {
                    String article = readFile(file);
                    words.addAll(processArticle(article));
                }
            }
        }
        
        Class.forName("com.mysql.jdbc.Driver");
        Connection connect = DriverManager.getConnection("jdbc:mysql://localhost:3306/newsaggregator?user=root&password=dininta");
        Statement statement = connect.createStatement();
        ResultSet resultSet = statement.executeQuery("select * from artikel");
        while (resultSet.next()) {
            String article = resultSet.getString("FULL_TEXT");
            words.addAll(processArticle(article));
        }
        
        System.out.println("Writing to text file...");
        try (Writer bw = new BufferedWriter(new FileWriter("resource/WordList.txt"))) {
            for (Word w : words) {
                bw.write(w.getText());
                bw.write("\t");
                bw.write(w.getSynsetType().toString());
                bw.write("\n");
            }
        }
    }
}
