package conceptlink;

import edu.smu.tspell.wordnet.SynsetType;
import java.util.Objects;
import org.apache.commons.lang3.builder.EqualsBuilder;

/**
 *
 * @author TOSHIBA PC
 */
public class Word
{
    private final String text;
    private final SynsetType synsetType;
    
    public Word(String text, String posTag)
    {
        this.text = text.toLowerCase();
        this.synsetType = getSynsetType(posTag);
    }
    public Word(Word w)
    {
        this.text = w.getText();
        this.synsetType = w.getSynsetType();
    }
    public String getText()
    {
        return this.text;
    }
    public SynsetType getSynsetType()
    {
        return this.synsetType;
    }
    public boolean haveSynset()
    {
        return this.synsetType != null;
    }
    @Override
    public String toString()
    {
        if (this.synsetType == null) {
            return this.text;
        }
        return this.text + "\t" + this.synsetType;
    }
    public static SynsetType getSynsetType(String posTag)
    {
        if (posTag.contains("NN") || posTag.contains("NOUN")|| posTag.contains("1")) {
            return SynsetType.NOUN;
        } else if (posTag.contains("VB") || posTag.equalsIgnoreCase("VERB")|| posTag.contains("2")) {
            return SynsetType.VERB;
        } else if (posTag.contains("JJ") || posTag.contains("ADJECTIVE")|| posTag.contains("3")) {
            return SynsetType.ADJECTIVE;
        } else if (posTag.contains("RB") && !posTag.contains("WRB") || posTag.equalsIgnoreCase("ADVERB")|| posTag.contains("4")) {
            return SynsetType.ADVERB;
        }
        return null;
    }
    @Override
    public boolean equals(Object o)
    {
        if (o == this) return true;
        if (!(o instanceof Word)) return false;

        Word w = (Word) o;
        return new EqualsBuilder()
                .append(text, w.text)
                .append(synsetType, w.synsetType)
                .isEquals();
    }
    @Override
    public int hashCode()
    {
        return Objects.hash(text, synsetType);
    }
}
