package conceptlink;

import be.abeel.util.HashMap2D;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author TOSHIBA PC
 */
public class Calculator
{
    private static HashMap2D<Word, Word, Double> wordSim;
    private static HashMap<String, Double> wordFreq;
    
    /* CONSTANT VARIABLE */
    private static final String WORD_FREQUENCY_FILE_NAME = "resource/WordFrequency.txt";
    private static final String WORD_SIMILARITY_FILE_NAME = "resource/WordSimilarityData.EN";
    private static final double THETA = 0.2;
    
    /**
     * Initialize WordFreq and WordSim map
     */
    public static void initialize() throws Exception
    {
        // Initialize WordFreq map
        Calculator.wordFreq = new HashMap<String, Double>();
        BufferedReader reader = new BufferedReader(new FileReader(WORD_FREQUENCY_FILE_NAME));
        String line = null;
        while ((line = reader.readLine()) != null ) {
            String[] data = line.split("\t");
            Calculator.wordFreq.put(data[0], Double.parseDouble(data[1]));
        }
        reader.close();
        
        // Initialize WordSim map
        System.out.println("\nInitialize WordSim map...");
        long lStartTime = System.nanoTime();
        Calculator.wordSim = new HashMap2D<Word, Word, Double>();
        reader = new BufferedReader(new FileReader(WORD_SIMILARITY_FILE_NAME));
        while ((line = reader.readLine()) != null ) {
            String[] data = line.split("\t");
            Word key1 = new Word(data[0], data[1]);
            Word key2 = new Word(data[2], data[3]);
            Calculator.wordSim.put(key1, key2, Double.parseDouble(data[4]));
        }
        reader.close();
        long lEndTime = System.nanoTime();
        System.out.println("\nDone initializing, size of wordsim: " + wordSim.size());
        System.out.println("Elapsed time in milliseconds (init wordsim): " + (lEndTime - lStartTime) / 1000000);
    }

    /**
     * Calculate word similarity
     */
    public static double wordSimilarity(Word w1, Word w2) throws Exception
    {
        if (w1.equals(w2)) {
            return 1;
        }
        
        if (wordSim.containsKey(w1, w2)) {
            return wordSim.get(w1, w2);
        } else if (wordSim.containsKey(w2, w1)) {
            return wordSim.get(w2, w1);
        }
        return 0;
    }
    
    /**
     * Calculate sentence similarity
     */
    public static double sentenceSimilarity(Sentence s1, Sentence s2) throws Exception
    {
        List<Word[]> CL = new ArrayList<>();
        List<Word> c1 = new ArrayList<>(s1.getConcepts());
        List<Word> c2 = new ArrayList<>(s2.getConcepts());
        
        // Get all pairs of concepts which similarity > THETA
        double max;
        Word firstConcept = null, secondConcept = null;
        do {
            max = 0;
            for (Word c1x : c1) {
                for (Word c2y : c2) {
                    double sim = wordSimilarity(c1x, c2y);
                    if (sim > max) {
                        max = sim;
                        firstConcept = c1x;
                        secondConcept = c2y;
                    }
                }
            }
            
            if (max > THETA) {
                CL.add(new Word[] {firstConcept, secondConcept});
                c1.remove(firstConcept);
                c2.remove(secondConcept);
            }
        } while (!c1.isEmpty() && !c2.isEmpty() && max > THETA);
        
        // Calculate similarity score
        double sim = 0;
        for (Word[] concepts : CL) {
            sim += wordFrequency(concepts[0]) * wordFrequency(concepts[1]) * wordSimilarity(concepts[0], concepts[1]);
        }
        return sim;
    }
    
    /**
     * Get word frequency (tf-idf)
     */
    public static double wordFrequency(Word w)
    {
        if (wordFreq.containsKey(w.getText())) {
            return wordFreq.get(w.getText());
        }
        return 0;
    }
}
