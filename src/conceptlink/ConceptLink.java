package conceptlink;

import IndonesianNLP.IndonesianPOSTagger;
import IndonesianNLP.IndonesianSentenceDetector;
import IndonesianNLP.IndonesianSentenceTokenizer;
import IndonesianNLP.IndonesianStemmer;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import net.sf.javailp.Constraint;
import net.sf.javailp.Linear;
import net.sf.javailp.OptType;
import net.sf.javailp.Problem;
import net.sf.javailp.ResultImpl;
import net.sf.javailp.Solver;
import net.sf.javailp.SolverFactory;
import net.sf.javailp.SolverFactoryGLPK;
import querysumm.Knapsack;
import querysumm.Summarizer;
import utils.CSVUtils;
import static weka.core.Utils.log2;

/**
 *
 * @author TOSHIBA PC
 */
public class ConceptLink extends Summarizer
{
    private List<Sentence> sentences;
    private double[][] sentenceSimilarity;
    private double[][] sentenceScore;
    private Sentence querySentence;
    
    /* CONSTANT VARIABLE */
    private static final boolean STEMMING = true;
    private static final String SENTENCE_SELECTION = "MMR";
    private static double SENTENCE_SIM_BOUND = 0.8;
    
    class SentenceSim implements Runnable
    {
        public int i, j;
        public SentenceSim(int i, int j)
        {
            this.i = i;
            this.j = j;
        }
        public void run()
        {
            try {
                double score = Calculator.sentenceSimilarity(sentences.get(i), sentences.get(j));
                sentenceSimilarity[i][j] = score;
                sentenceSimilarity[j][i] = score;
            } catch(Exception e) {}
        }
    }
    
    /**
     * Constructor
     */
    public ConceptLink(List<String> articles, String query) throws Exception
    {
        super(articles, query);
        Calculator.initialize();
    }

    /**
     * Build the summary
     */
    public String buildSummary() throws Exception
    {
        long lStartTime, lEndTime;
        
        // Pre process
        System.out.println("\nPreprocessing...");
        lStartTime = System.nanoTime();
        preProcess();
        lEndTime = System.nanoTime();
        System.out.println("\nDone preprocessing, num of sentences: " + sentences.size());
        System.out.println("Elapsed time in milliseconds (preprocess): " + (lEndTime - lStartTime) / 1000000);
        
        // Delete redundancy if using DP2
        if (SENTENCE_SELECTION.equalsIgnoreCase("DP2")) {
            List<Integer> deletedIdx = new ArrayList<>();
            for (int i = 0; i < sentences.size()-1; i++) {
                for (int j = i+1; j < sentences.size(); j++) {
                    if (cosineSimilarity(i,j) > SENTENCE_SIM_BOUND) {
                        deletedIdx.add(i);
                        j = sentences.size();
                    }
                }
            }
            int[] array = new int[deletedIdx.size()];
            for (int i = 0; i < deletedIdx.size(); i++) array[i] = deletedIdx.get(i);
            for (int i = array.length-1; i >= 0; i--) {
                sentences.remove(array[i]);
            }
        }
        
        // Calculate all sentence similarity
        System.out.println("\nCalculating all sentence similarity...");
        sentenceSimilarity = new double[sentences.size()][sentences.size()];
        ExecutorService executor = Executors.newFixedThreadPool(10);
        lStartTime = System.nanoTime();
        for (int i = 0; i < sentences.size()-1; i++) {
            for (int j = i+1; j < sentences.size(); j++) {
                Runnable worker = new SentenceSim(i, j);  
                executor.execute(worker);
            }
        }
        executor.shutdown();
        try {
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {}
        lEndTime = System.nanoTime();
        System.out.println("Elapsed time in milliseconds (sentence sim): " + (lEndTime - lStartTime) / 1000000);
        
        // Sentence scoring (QIScore)
        System.out.println("\nCalculating all sentence score...");
        sentenceScore = new double[sentences.size()][2];
        lStartTime = System.nanoTime();
        for (int i = 0; i < sentences.size(); i++) {
            sentenceScore[i][0] = QIScore(i);
        }
        lEndTime = System.nanoTime();
        System.out.println("Elapsed time in milliseconds (sentence scoring QIScore): " + (lEndTime - lStartTime) / 1000000);
        
        // Sentence scoring (QFocus)
        lStartTime = System.nanoTime();
        for (int i = 0; i < sentences.size(); i++) {
            sentenceScore[i][1] = QFocus(i);
        }
        lEndTime = System.nanoTime();
        System.out.println("Elapsed time in milliseconds (sentence scoring QFocus): " + (lEndTime - lStartTime) / 1000000);
        
        // Print result
        String csvFile = "result.csv";
        FileWriter writer = new FileWriter(csvFile);
        CSVUtils.writeLine(writer, Arrays.asList("Indeks", "QIScore", "QFocus", "Jumlah konsep", "Kalimat"));
        for (int i = 0; i < sentences.size(); i++) {
            CSVUtils.writeLine(writer, Arrays.asList(
                    Integer.toString(i),
                    Double.toString(sentenceScore[i][0]),
                    Double.toString(sentenceScore[i][1]),
                    Integer.toString(sentences.get(i).getNumConcepts()),
                    sentences.get(i).getText()
            ));
        }
        writer.flush();
        writer.close();
//        System.out.println("\nRESULT (SENTENCE SIMILARITY)");
//        for (int i = 0; i < sentences.size(); i++) {
//            for (int j = 0; j < sentences.size(); j++) {
//                System.out.print(this.sentenceSimilarity[i][j]);
//                System.out.print("\t");
//            }
//            System.out.println();
//        }
        
        // Sentence selection
        lStartTime = System.nanoTime();
        List<Sentence> summary;
        if (SENTENCE_SELECTION.equalsIgnoreCase("MMR")) {
            summary = MMR();
        } else if (SENTENCE_SELECTION.equalsIgnoreCase("ILP")) {
            summary = ILP();
        } else {
            summary = DP();
        }
        lEndTime = System.nanoTime();
        System.out.println("Elapsed time in milliseconds (MMR): " + (lEndTime - lStartTime) / 1000000);
        
        // Return summary
        StringBuilder sb = new StringBuilder();
        for (Sentence s : summary) {
            sb.append(s.getText()).append("\n");
        }
        return sb.toString();
    }
    
    /**
     * 
     */
    private double cosineSimilarity(int i, int j)
    {
        HashSet<Word> words = new HashSet<>();
        for (Word w : sentences.get(i).getConcepts()) {
            words.add(w);
        }
        for (Word w : sentences.get(j).getConcepts()) {
            words.add(w);
        }
        
        double[] docVector1 = new double[words.size()], docVector2 = new double[words.size()];
        int idx = 0;
        for (Word word : words) {
            docVector1[idx] = 0;
            for (Word w : sentences.get(i).getConcepts()) {
                if (w.equals(word)) docVector1[idx]++;
            }
            docVector2[idx] = 0;
            for (Word w : sentences.get(j).getConcepts()) {
                if (w.equals(word)) docVector2[idx]++;
            }
            idx++;
        }
        
        double dotProduct = 0.0;
        double magnitude1 = 0.0;
        double magnitude2 = 0.0;
        double cosineSimilarity = 0.0;

        for (int k = 0; k < docVector1.length; k++) {
            dotProduct += docVector1[k] * docVector2[k];  //a.b
            magnitude1 += Math.pow(docVector1[k], 2);  //(a^2)
            magnitude2 += Math.pow(docVector2[k], 2); //(b^2)
        }

        magnitude1 = Math.sqrt(magnitude1);//sqrt(a^2)
        magnitude2 = Math.sqrt(magnitude2);//sqrt(b^2)

        if (magnitude1 != 0.0 | magnitude2 != 0.0) {
            cosineSimilarity = dotProduct / (magnitude1 * magnitude2);
        } else {
            return 0.0;
        }
        return cosineSimilarity;
    }
    
    /**
     * Pre process
     */
    private void preProcess() throws Exception
    {
        this.sentences = new ArrayList<Sentence>();
        IndonesianSentenceDetector stDetector = new IndonesianSentenceDetector();
        IndonesianPOSTagger posTagger = new IndonesianPOSTagger();
        IndonesianSentenceTokenizer tokenizer = new IndonesianSentenceTokenizer();
        IndonesianStemmer stemmer = new IndonesianStemmer();
        
        for (String article : this.articles) {
            // Sentence delimitter
            List<String> rawSentences = stDetector.splitSentence(article);
            
            for (String sentence : rawSentences) {
                // Tokenization 
                List<String> tokens = tokenizer.tokenizeSentence(sentence);
                // Pos Tag
                List<String[]> posTagged = posTagger.doPOSTag(String.join(" ", tokens));
                // Stemming
                if (this.STEMMING) {
                    for (String[] word : posTagged) {
                        word[0] = stemmer.stem(word[0]);
                    }
                }
                
                sentences.add(new Sentence(sentence, posTagged));
            }
        }
        
        // Pre process query
        List<String> tokens = tokenizer.tokenizeSentence(this.query);
        List<String[]> posTagged = posTagger.doPOSTag(String.join(" ", tokens));
        if (this.STEMMING) {
            for (String[] word : posTagged) {
                word[0] = stemmer.stem(word[0]);
            }
        }
        this.querySentence = new Sentence(this.query, posTagged);
    }
    
    /**
     * Pre process
     */
    private void preProcess2() throws Exception
    {
        this.sentences = new ArrayList<Sentence>();
        IndonesianSentenceDetector stDetector = new IndonesianSentenceDetector();
        IndonesianPOSTagger posTagger = new IndonesianPOSTagger();
        IndonesianSentenceTokenizer tokenizer = new IndonesianSentenceTokenizer();
        IndonesianStemmer stemmer = new IndonesianStemmer();
        
        List<List<String>> rawSentences = new ArrayList<>();
        for (String article : this.articles) {
            // Sentence delimitter
            rawSentences.add(stDetector.splitSentence(article));
        }
        int i = 0;
        while (!rawSentences.isEmpty()) {
            if (!rawSentences.get(i).isEmpty()) {
                String sentence = rawSentences.get(i).get(0);
                // Tokenization 
                List<String> tokens = tokenizer.tokenizeSentence(sentence);
                // Pos Tag
                List<String[]> posTagged = posTagger.doPOSTag(String.join(" ", tokens));
                // Stemming
                if (this.STEMMING) {
                    for (String[] word : posTagged) {
                        word[0] = stemmer.stem(word[0]);
                    }
                }
                
                sentences.add(new Sentence(sentence, posTagged));
                rawSentences.get(i).remove(sentence);
                i = (i+1) % rawSentences.size();
            } else {
                List<String> temp = rawSentences.get(i);
                rawSentences.remove(temp);
                if (i >= rawSentences.size()) i = 0;
            }
        }
        
        // Pre process query
        List<String> tokens = tokenizer.tokenizeSentence(this.query);
        List<String[]> posTagged = posTagger.doPOSTag(String.join(" ", tokens));
        if (this.STEMMING) {
            for (String[] word : posTagged) {
                word[0] = stemmer.stem(word[0]);
            }
        }
        this.querySentence = new Sentence(this.query, posTagged);
    }
    
    /**
     * Calculate the representation score of a sentence
     * The more representative the sentence of whole documents, the higher score it gets
     */
    private double QIScore(int sentenceIdx)
    {
        double score = 0;
        for (int i = 0; i < this.sentences.size(); i++) {
            if (i != sentenceIdx) {
                score += log2(this.sentences.get(sentenceIdx).getNumConcepts() + 1)
                        * log2(this.sentences.get(i).getNumConcepts() + 1)
                        * sentenceSimilarity[sentenceIdx][i];
            }
        }
        return score / (this.sentences.size()-1);
    }
    
    /**
     * Calculate the query-relevant score of a sentence
     */
    private double QFocus(int sentenceIdx) throws Exception
    {
        return log2(sentences.get(sentenceIdx).getNumConcepts() + 1)
                * log2(querySentence.getNumConcepts() + 1)
                * Calculator.sentenceSimilarity(sentences.get(sentenceIdx), querySentence);
    }

    /**
     * Sentence selection using MMR
     */
    private List<Sentence> MMR()
    {
        List<Sentence> summary = new ArrayList<>();
        List<Integer> selectedIdx = new ArrayList<>();
        int length = 0;
        
        while (selectedIdx.size() != sentences.size()) {
            double max = 0;
            int idxMax = 0;
            for (int i = 0; i < sentences.size(); i++) {
                if (!selectedIdx.contains(i)) {
                    double penalty = 0;
                    for (int idx : selectedIdx) {
                        if (penalty < sentenceSimilarity[i][idx])
                            penalty = sentenceSimilarity[i][idx];
                    }
                    
                    double score = LAMBDA * sentenceScore[i][0] + (1 - LAMBDA) * sentenceScore[i][1] - penalty;
                    
                    if (score > max) {
                        max = score;
                        idxMax = i;
                    }
                }
            }
            length += sentences.get(idxMax).getNumWords();
            if (length <= SUMMARY_LENGTH) {
                summary.add(sentences.get(idxMax));
                System.out.println("Selected sentence's index: " + idxMax);
                selectedIdx.add(idxMax);
            } else {
                break;
            }
        }
        
        return summary;
    }
    
    /**
     * Sentence selection using ILP
     */
    private List<Sentence> ILP()
    {
        int numSentences = this.sentences.size();
        
        SolverFactory factory = new SolverFactoryGLPK();
        factory.setParameter(Solver.VERBOSE, 0);
        Problem problem = new Problem();
        
        // Objective function
        Linear linear = new Linear();
        for (int i=0; i < numSentences; i++) {
            String varName = "x".concat("_").concat(String.valueOf(i));
            linear.add(LAMBDA * sentenceScore[i][0] + (1 - LAMBDA) * sentenceScore[i][1], varName);
        }
        for (int i=0; i < numSentences-1; i++) {
            for (int j=i+1; j < numSentences; j++) {
                String varName = "x".concat("_").concat(String.valueOf(i)).concat("_").concat(String.valueOf(j));
                linear.add(-1 * sentenceSimilarity[i][j], varName);
            }
        }
        problem.setObjective(linear, OptType.MAX);

        // Constraint panjang ringkasan
        int counter = 0;
        linear = new Linear();
        for (int i=0; i < numSentences; i++) {
            String varName = "x".concat("_").concat(String.valueOf(i));
            linear.add(sentences.get(i).getNumWords(), varName);
        }
        problem.add(new Constraint(String.valueOf(counter), linear, "<=", SUMMARY_LENGTH));
        counter++;

        // Constraint lainnya
        for (int i=0; i < numSentences-1; i++) {
            for (int j=i+1; j < numSentences; j++) {
                // Constraint aij - ai <= 0
                linear = new Linear();
                linear.add(1, "x".concat("_").concat(String.valueOf(i)).concat("_").concat(String.valueOf(j)));
                linear.add(-1, "x".concat("_").concat(String.valueOf(i)));
                problem.add(new Constraint(String.valueOf(counter), linear, "<=", 0));
                counter++;
                
                // Constraint aij - aj <= 0
                linear = new Linear();
                linear.add(1, "x".concat("_").concat(String.valueOf(i)).concat("_").concat(String.valueOf(j)));
                linear.add(-1, "x".concat("_").concat(String.valueOf(j)));
                problem.add(new Constraint(String.valueOf(counter), linear, "<=", 0));
                counter++;
                
                // Constraint ai + aj - aij <= 1
                linear = new Linear();
                linear.add(-1, "x".concat("_").concat(String.valueOf(i)).concat("_").concat(String.valueOf(j)));
                linear.add(1, "x".concat("_").concat(String.valueOf(i)));
                linear.add(1, "x".concat("_").concat(String.valueOf(j)));
                problem.add(new Constraint(String.valueOf(counter), linear, "<=", 1));
                counter++;
            }
        }
        
        // 0 <= x <= 1, x adalah integer
        for (int i=0; i < numSentences; i++) {
            linear = new Linear();
            String varName = "x".concat("_").concat(String.valueOf(i));
            linear.add(1, varName);
            problem.add(new Constraint(String.valueOf(counter), linear, ">=", 0));
            counter++;
            problem.add(new Constraint(String.valueOf(counter), linear, "<=", 1));
            counter++;
            
            problem.setVarType(varName, Integer.class);
        }
        for (int i=0; i < numSentences-1; i++) {
            for (int j=i+1; j < numSentences; j++) {
                linear = new Linear();
                String varName = "x".concat("_").concat(String.valueOf(i)).concat("_").concat(String.valueOf(j));
                linear.add(1, varName);
                problem.add(new Constraint(String.valueOf(counter), linear, ">=", 0));
                counter++;
                problem.add(new Constraint(String.valueOf(counter), linear, "<=", 1));
                counter++;
            
                problem.setVarType(varName, Integer.class);
            }
        }

        System.out.println("Solving ILP problem...");
        Solver solver = factory.get();
        ResultImpl result =  (ResultImpl) solver.solve(problem);
        System.out.println(problem);
        
        List<Sentence> summary = new ArrayList<>();
        System.out.println("Selected sentence:");
        for (int i = 0; i < numSentences; i++) {
            if (result.get("x".concat("_").concat(String.valueOf(i))).intValue() == 1) {
                System.out.println(i);
                summary.add(sentences.get(i));
            }
        }
        return summary;
    }
    
    /**
     * Sentence selection using DP
     */
    private List<Sentence> DP()
    {
        int sentenceWeight[] = new int[this.sentences.size()];
        for (int i = 0; i < this.sentences.size(); i++) {
            sentenceWeight[i] = this.sentences.get(i).getNumWords();
        }
        double finalScore[] = new double[this.sentences.size()];
        for (int i = 0; i < this.sentences.size(); i++) {
            finalScore[i] = LAMBDA * sentenceScore[i][0] + (1 - LAMBDA) * sentenceScore[i][1];
        }
        
        Knapsack problem = new Knapsack(SUMMARY_LENGTH, sentenceWeight, finalScore);
        problem.solve();
        List<Integer> selectedIdx = problem.getSelectedItem();
        List<Sentence> summary = new ArrayList<>();
        for (int i : selectedIdx) {
            System.out.println("Selected sentence's index: " + (i-1));
            summary.add(sentences.get(i-1));
        }
        
        return summary;
    }
}
