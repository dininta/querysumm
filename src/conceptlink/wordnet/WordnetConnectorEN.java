package conceptlink.wordnet;

import conceptlink.Word;
import edu.smu.tspell.wordnet.NounSynset;
import edu.smu.tspell.wordnet.Synset;
import edu.smu.tspell.wordnet.SynsetType;
import edu.smu.tspell.wordnet.WordNetDatabase;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import translator.Translator;

/**
 *
 * @author TOSHIBA PC
 */
public class WordnetConnectorEN
{
    private static WordNetDatabase db;
    
    /**
     * Get word's definition (synonym, hypernym, meronym, gloss) from WordNet
     */
    public static HashSet<String> getWordDefinition(Word word) throws Exception
    {
        if (db == null) {
            System.setProperty("wordnet.database.dir", "C:\\Program Files (x86)\\WordNet\\2.1\\dict");
            db = WordNetDatabase.getFileInstance();
        }
        
        HashSet<String> definition = new HashSet<String>();
        
        ArrayList<String> enWord = Translator.getTranslation(word.getText());
        if (enWord == null) {
            return definition;
        }
            
        Synset[] synsets = db.getSynsets(enWord.get(0), word.getSynsetType());
        if (synsets.length == 0) {
            definition.add(enWord.get(0));
            return definition;
        }
            
        for (Synset syn : synsets) {
            // Get gloss
            definition.add(syn.getDefinition());
            // Get synonym
            String[] synonyms = syn.getWordForms();
            for (String w : synonyms) {
                definition.add(w);
            }
            // Get hypernym & meronym
            if (word.getSynsetType() == SynsetType.NOUN) {
                List<String> hypernyms = getWordsFromNounSynset(((NounSynset) syn).getHypernyms());
                List<String> memberMeronyms = getWordsFromNounSynset(((NounSynset) syn).getMemberMeronyms());
                List<String> partMeronyms = getWordsFromNounSynset(((NounSynset) syn).getPartMeronyms());
                List<String> subMeronyms = getWordsFromNounSynset(((NounSynset) syn).getSubstanceMeronyms());
                definition.addAll(hypernyms);
                definition.addAll(memberMeronyms);
                definition.addAll(partMeronyms);
                definition.addAll(subMeronyms);
            }
        }
        
        return definition;
    }
    
    /**
     * Convert array of NounSynset to list of string
     */
    private static List<String> getWordsFromNounSynset(NounSynset[] nounSynsets)
    {
        List<String> result = new ArrayList<String>();
        for (NounSynset ns : nounSynsets) {
            String[] words = ns.getWordForms();
            for (String w : words) {
                result.add(w);
            }
        }
        return result;
    }
}
