package conceptlink.wordnet;

import conceptlink.Word;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/**
 *
 * @author TOSHIBA PC
 */
public class WordnetConnectorID
{
    public static String WORDNET_ID_FILE_NAME = "resource/bahasa-wordnet.syns";
    public static Map<Word, List<String>> db;
    
    /**
     * Get word's definition (synonym and gloss) from WordNet ID
     */
    public static HashSet<String> getWordDefinition(Word word) throws IOException
    {
        if (db == null) {
            initDb();
        }
        HashSet<String> result;
        if (db.containsKey(word))
            result = new HashSet<String>(db.get(word));
        else {
            result = new HashSet<String>();
            result.add(word.getText());
        }
        return result;
    }
    
    /**
     * Initialize map
     */
    public static void initDb() throws IOException
    {
        db = new HashMap<Word, List<String>>();
        BufferedReader reader = new BufferedReader(new FileReader(WORDNET_ID_FILE_NAME));
        
        String line = null;
        while ((line = reader.readLine()) != null ) {
            if (line.matches("^CAT\t::\t.*$")) {
                // get posTag
                String posTag = line.replace("CAT\t::\t", "").trim();
                // get gloss
                line = reader.readLine();
                String gloss = line.replace("CONCEPT\t::\t", "").trim();
                // get synset
                line = reader.readLine();
                line = reader.readLine();
                String[] synset = line.replace("SYNSET-BAHASA\t::\t", "").trim().split(", ");
                
                // insert to db
                for (String w : synset) {
                    Word key = new Word(w, posTag);
                    List<String> value = new ArrayList<String>();
                    if (!gloss.contains("N/A"))
                        value.add(gloss);
                    value.addAll(Arrays.asList(synset));
                    if (db.containsKey(key)) {
                        for (String v : value)
                            if (!db.get(key).contains(v))
                                db.get(key).add(v);
                    } else {
                        db.put(key, value);
                    }
                }
            }
        }
    }
}
