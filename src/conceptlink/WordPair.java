package conceptlink;

/**
 *
 * @author TOSHIBA PC
 */
public class WordPair
{
    public Word w1;
    public Word w2;
    public WordPair(Word w1, Word w2)
    {
        this.w1 = w1;
        this.w2 = w2;
    }
    public Word getFirstWord()
    {
        return this.w1;
    }
    public Word getSecondWord()
    {
        return this.w2;
    }
    @Override
    public boolean equals(Object o)
    {
        if (o == this) return true;
        if (!(o instanceof WordPair)) return false;

        WordPair wp = (WordPair) o;
        return (this.w1.equals(wp.w1) && this.w2.equals(wp.w2)) ||
                (this.w1.equals(wp.w2) && this.w2.equals(wp.w1));
    }
    @Override
    public int hashCode()
    {
        return 0;
    }
    @Override
    public String toString()
    {
        return this.w1.toString() + " dan " + this.w2.toString();
    }
}
