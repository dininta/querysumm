package conceptlink;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author TOSHIBA PC
 */
public class Sentence
{
    private String text;
    private List<Word> concepts;
    
    public Sentence(String text, List<String[]> words) throws Exception
    {
        this.text = text;
        this.concepts = new ArrayList<Word>();
        for (String[] w : words) {
            Word concept = new Word(w[0], w[1]);
            if (concept.haveSynset()) {
                this.concepts.add(concept);
            }
        }
    }
    public String getText()
    {
        return this.text;
    }
    public List<Word> getConcepts()
    {
        return this.concepts;
    }
    public int getNumConcepts()
    {
        return this.concepts.size();
    }
    public int getNumWords()
    {
        return this.text.split(" ").length;
    }
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append(this.text).append("\n");
        for (Word w : concepts) {
            sb.append(w).append("\n");
        }
        return sb.toString();
    }
    public boolean equals(Sentence s)
    {
        return this.text.equals(s.getText());
    }
}
